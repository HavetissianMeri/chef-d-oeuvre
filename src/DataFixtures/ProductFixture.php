<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use App\Entity\Product;

class ProductFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create("fr_FR");

        for($i = 1; $i <= 5; $i++) {
            $category = new Category();
            $category->setName($faker->sentence());

            $manager->persist($category);

            for($a = 1; $a <= mt_rand(3, 5); $a++) {
            $product = new Product();

                $content = "<p>" . join($faker->paragraphs(2), "</p><p>") . "</p>";

                $product->setName($faker->sentence())
                        ->setPrice(mt_rand(1, 50))
                        ->setDescription($content)
                        ->setImage($faker->image($dir = 'public/build/images', $width = 640, $height = 480))
                        ->setCategory($category);

                $manager->persist($product);
            }
        }

        $manager->flush();
    }
}
