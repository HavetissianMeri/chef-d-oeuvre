<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserNoPSWDType;
use App\Repository\OrderHistoryRepository;

class PanelController extends Controller
{
    /**
     * @Route("/user/panel", name="panel_user")
     * @Route("/user/panel/{id}/edit", name="edit_user")
     */
    public function user(UserRepository $repo, Request $req, User $user = null, OrderHistoryRepository $orders)
    {
        // $id  = $activeUser->getId();
        // $user = $repo->find($id);
        $user = $this->getUser();
        $activeUser =$this->getUser();

        $orders = $orders->findAll();
        foreach($orders as $line) {
            if($line->getShoppingCart()->getUser()->getId() == $user->getId()) {
                $order[] = $line;
            }
        }
        
        $form = $this->createForm(UserNoPSWDType::class, $user);

        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {

            dump($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            

        }
        
        return $this->render('panel/user.html.twig', [
            'controller_name' => 'PanelController',
            'activeUser'=>$user,
            'form'=>$form->createView(),
            "orders" => $order
        ]);
    }


        /**
     * @Route("/admin/panel", name="adminPanel")
     */
    public function admin(UserRepository $repo)
    {

        $user = $repo->findAll();
        dump($this->getUser());

        return $this->render('panel/admin.html.twig', [
            'controller_name' => 'PanelController',
            'user'=>$user,
        ]);
    }
}
