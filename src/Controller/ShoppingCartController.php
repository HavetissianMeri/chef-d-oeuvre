<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ShoppingCart;
use App\Entity\OrderHistory;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Entity\User;
use App\Repository\ShoppingCartRepository;
use Symfony\Component\HttpFoundation\Request;

class ShoppingCartController extends Controller
{
    /**
     * @Route("/user/shopping-cart", name="shopping_cart")
     */
    public function index(ShoppingCartRepository $repo, SessionInterface $session, ObjectManager $manager, UserInterface $user, Request $request)
    {
        $active = true;
        $cart = $repo->findOneBy(array('active' => $active));

        if(!$cart) {
            $cart = $session->get("cart");
            if(!$cart){
                $cart = new ShoppingCart();
                $cart->setUser($user);
            }
        }

            $totalPrice = 0;
            foreach($cart->getProductLines() as $line) {
                $totalPrice = $totalPrice + $line->getPrice();
            }

            $verifCart = false;

            if(count($cart->getProductLines()) > 0) {
                $verifCart = true;
            }

            $cart = $manager->merge($cart);
            $productLine = $cart->getProductLines();

            if ($request->isMethod('POST')) {
                $cart->setActive(true);
                dump($cart->getActive());
            }


            $manager->persist($cart);
            $manager->flush();

            $session->set("cart", $cart);
            
        return $this->render('shopping_cart/index.html.twig', [
            'controller_name' => 'ShoppingCartController',
            "productLine" => $productLine,
            "totalPrice" => $totalPrice,
            "verifCart" => $verifCart
        ]);
    }

    /**
     * @Route("/user/order-adress", name="order_adress")
     */
    public function order_adress(SessionInterface $session)
    {
            $cart = $session->get("cart");

            $cart = $this->getDoctrine()->getManager()->merge($cart);

            $user = $this->getDoctrine()->getRepository(User::class)->find($cart->getUser()->getId());
            $number_adress = $user->getNumber();
            $street = $user->getStreet();
            $city = $user->getCity();
            $state = $user->getState();
            $zipcode = $user->getZipcode();

            $adress = $number_adress . " " . $street . ", " . $city . ", " . $state . ", " . $zipcode;
            
        return $this->render('shopping_cart/order_adress.html.twig', [
            "adress" => $adress
        ]);
    }

    /**
     * @Route("/user/payment", name="payment")
     */
    public function payment(Session $session, ObjectManager $manager)
    {
            $history = new OrderHistory;

            $session->start();

            $cart = $session->get("cart");

            $cart = $this->getDoctrine()->getManager()->merge($cart);

            $cart->setActive(false);

            $history->setShoppingCart($cart);
            $history->setDate(new \DateTime());

            $manager->persist($history);
            $manager->flush();

            $session->remove("cart");
            
        return $this->render('shopping_cart/payment.html.twig', [
        ]);
    }
}
