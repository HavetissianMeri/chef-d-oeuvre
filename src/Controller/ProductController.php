<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\FileUploader;
use App\Form\ProductType;

class ProductController extends Controller
{
    /**
     * @Route("/product", name="product")
     */
    public function index(ProductRepository $repo, Request $request)
    {
        $products = $repo->findAll();

        if ($request->isMethod('POST')) {
            $products = $this->getDoctrine()
                ->getRepository(Product::class)
                ->findAllByName($request->get("nameProduct"));
        }

        
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            "products" => $products,
            "imageURI" => $this->getParameter('images_URI'),
        ]);
    }

    /**
     *  @Route("/admin/create-product", name="create_product")
     *  @Route("/admin/{id}/edit-product", name="edit_product")
     */
    public function form(Product $product = null, Request $req, ObjectManager $manager, FileUploader $fileUploader){

        if(!$product) {
            $product = new Product();
        }

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()) {

            $file = $product->getImage();

            $fileName = $fileUploader->upload($file);

            $product->setImage($fileName);

            $manager->persist($product);
            $manager->flush();

            return $this->redirectToRoute("product", ["id" => $product->getId()]);
        }
        
        return $this->render("product/product.html.twig", ["formProduct" => $form->createView(),
        'editMode' => $product->getId() ==! null]);
    }

    /**
     *  @Route("/product/{id}", name="show_product")
     */
    public function show(Product $product, Request $request){

        if ($request->isMethod('POST')) {

            $quantity = $request->get("quantity");
    
            return $this->redirectToRoute("product_line", ["id" => $product->getId(), "quantity" => $quantity]);
        }

        return $this->render("product/show.html.twig", ["product" => $product, "imageURI" => $this->getParameter('images_URI')]);
    }

    /**
    *  @Route("/admin/{id}/remove-product", name="remove_product")
    */
    public function remove(Product $product) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute("product", []);
    }
}
