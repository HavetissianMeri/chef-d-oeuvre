<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;

class UserController extends Controller
{
    /**
     * @Route("/admin/user/remove/{id}", name="remove_user", defaults={"id":0})
     */
    public function index(UserRepository $repo, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $toDelete = $repo->find($id);
        $em->remove($toDelete);
        $em->flush();

        $user = $repo->findAll();
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            "user" => $user
        ]);
    }

    /**
     * @Route("/admin/getUser", name="userName")
     */
    public function getUsername()
    {
        # code...

        return $this->render('features/userNav.html.twig', [
            'controller_name' => 'UserController',
            "user" => $this->getUser()
        ]);

    }
}
