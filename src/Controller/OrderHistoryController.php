<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\OrderHistoryRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class OrderHistoryController extends Controller
{
    /**
     * @Route("/user/order-history/{id}", name="order_history")
     */
    public function index(OrderHistoryRepository $repo, UserInterface $user, int $id)
    {
        $user = $user->getId();
        $order = $repo->find($id);

        if($order->getShoppingCart()->getUser()->getId() == $user) {
            $validUser = true;
        }
        else {
            $validUser = false;
        }

        $totalprice = 0;
        foreach($order->getShoppingCart()->getProductLines() as $line) {
            $priceline = $line->getPrice();
            $totalprice = $totalprice + $priceline;
        }

        return $this->render('order_history/index.html.twig', [
            "order" => $order,
            "totalprice" => $totalprice,
            "validUser" => $validUser
        ]);
    }
}
