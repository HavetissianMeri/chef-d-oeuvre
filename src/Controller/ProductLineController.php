<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\ProductLine;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\ShoppingCart;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\OrderHistory;

class ProductLineController extends Controller
{
    /**
     * @Route("/user/{id}/{quantity}/product-line", name="product_line")
     */
    public function index(UserInterface $user, Session $session = null, int $quantity, int $id, ProductRepository $product, ObjectManager $manager)
    {
        if(!$session) {
        $session = new Session();
        }
        $session->start();

        $cart = $session->get("cart");

        if(!$cart) {
            $cart = new ShoppingCart();
        } else {
            $cart = $manager->merge($cart);
        }

        $product = $product->find($id);

        $productLine = null;

        foreach($cart->getProductLines() as $line) {
            if($line->getProduct()->getId() == $id) {
                $productLine = $line;
                $productLine->setQuantity($line->getQuantity() + $quantity);
                $price = $product->getPrice() * $productLine->getQuantity();
                $productLine->setPrice($price);
            }
        }
        if(!$productLine) {
        $productLine = new ProductLine();
        
        $productLine->setProduct($product);

        $productLine->setQuantity($quantity);
        
        $price = $product->getPrice() * $quantity;

        $productLine->setPrice($price);
        }

        $cart->addProductLine($productLine);

        $cart->setUser($user);
        
        $session->set("cart", $cart);

        $manager->persist($cart);
        $manager->flush();

        return $this->redirectToRoute("product", []);
    }

    /**
    *  @Route("/user/{id}/remove-productLine", name="remove_productLine")
    */
    public function remove(ProductLine $productLine) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($productLine);
        $em->flush();

        return $this->redirectToRoute("shopping_cart", []);
    }
}
